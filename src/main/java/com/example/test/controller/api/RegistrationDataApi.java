package com.example.test.controller.api;

import com.example.test.model.RegistrationDataDto;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.concurrent.CompletionStage;

@Api(value = "registration data api", tags = {"registrationData"})
public interface RegistrationDataApi {

    @ApiOperation(value = "Зарегистрировать пользователя", nickname = "register", notes = "", tags = {"registrationData",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Команда успешно выполнена"),
            @ApiResponse(code = 400, message = "Некорректные данные"),
            @ApiResponse(code = 401, message = "Неавторизованный доступ"),
            @ApiResponse(code = 409, message = "Конфликт"),
            @ApiResponse(code = 500, message = "Не удалось добавить класс обьекта"),
    })
    @PostMapping(value = "/registrationData", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    CompletionStage<Void> register(@ApiParam(value = "Данные пользователя", required = true) @Valid @RequestBody RegistrationDataDto registrationDataDto);
}
