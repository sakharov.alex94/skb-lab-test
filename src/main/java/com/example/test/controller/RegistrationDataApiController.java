package com.example.test.controller;

import com.example.test.controller.api.RegistrationDataApi;
import com.example.test.model.RegistrationDataDto;
import com.example.test.services.RegistrationDataService;
import com.example.test.services.converters.RegistrationDataConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.server.ResponseStatusException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Controller
@CrossOrigin(origins = "*")
public class RegistrationDataApiController implements RegistrationDataApi {

    private final RegistrationDataService registrationDataService;
    private final RegistrationDataConverter registrationDataConverter;

    @Autowired
    public RegistrationDataApiController(RegistrationDataService registrationDataService, RegistrationDataConverter registrationDataConverter) {
        this.registrationDataService = registrationDataService;
        this.registrationDataConverter = registrationDataConverter;
    }

    @Override
    public CompletionStage<Void> register(RegistrationDataDto registrationDataDto) {
        //сделаем допущение, что валидация всех остальных полей происходит на фронте
        if (registrationDataDto.getLogin().isBlank()) {
            return CompletableFuture.failedFuture(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Логин должен быть указан"));
        }
        var registrationData = registrationDataConverter.convert(registrationDataDto);
        return registrationDataService.register(registrationData);
    }
}
