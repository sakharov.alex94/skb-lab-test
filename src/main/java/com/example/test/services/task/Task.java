package com.example.test.services.task;

import java.util.UUID;

public abstract class Task {
    protected UUID id;

    public UUID getId() {
        return id;
    }

    public Task(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var task = (Task) o;
        return id.equals(task.getId());
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
