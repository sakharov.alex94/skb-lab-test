package com.example.test.services.task;

import com.example.test.model.*;
import com.example.test.repository.RegistrationDataRepository;
import com.example.test.services.DateTimeService;
import com.example.test.services.MessagingService;
import com.example.test.services.SendMailer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeoutException;

@Component
public class RegistrationDataExecutor implements TaskExecutor<RegistrationDataTask> {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationDataExecutor.class);

    private final MessagingService messagingService;
    private final SendMailer sendMailer;
    private final RegistrationDataRepository registrationDataRepository;
    private final DateTimeService dateTimeService;
    private final RetryTemplate messageServiceRetryTemplate;
    private final RetryTemplate mailServiceRetryTemplate;

    @Autowired
    public RegistrationDataExecutor(MessagingService messagingService, SendMailer sendMailer, RegistrationDataRepository registrationDataRepository, DateTimeService dateTimeService,
                                    @Qualifier("message-service") RetryTemplate messageServiceRetryTemplate,
                                    @Qualifier("mail-service") RetryTemplate mailServiceRetryTemplate) {
        this.messagingService = messagingService;
        this.sendMailer = sendMailer;
        this.registrationDataRepository = registrationDataRepository;
        this.dateTimeService = dateTimeService;
        this.messageServiceRetryTemplate = messageServiceRetryTemplate;
        this.mailServiceRetryTemplate = mailServiceRetryTemplate;
    }

    @Override
    public void execute(RegistrationDataTask task) {
        Status status;
        var emailContent = new EmailContent();
        var currentRegData = task.getRegistrationData();
        try {
            var response = getMessageServiceResponse(task);
            status = response.getStatus();
            updateRegistrationData(status, emailContent, currentRegData);
        } catch (TimeoutException e) {
            logger.info("Истекло время ожидания ответа от внешней системы");
        }
        try {
            sendMail(emailContent, currentRegData);
        } catch (TimeoutException e) {
            logger.info("Истекло время ожидания ответа от сервиса почты");
        }
    }

    private Message<Object> getMessageServiceResponse(RegistrationDataTask task) throws TimeoutException {
        return messageServiceRetryTemplate.execute(context -> messagingService.receive(task.getMessageId()));
    }

    private void sendMail(EmailContent emailContent, RegistrationData currentRegData) throws TimeoutException {
        mailServiceRetryTemplate.execute(context -> {
            sendMailer.sendMail(new EmailAddress(currentRegData.getEmail()), emailContent);
            return null;
        });
    }

    private void updateRegistrationData(Status status, EmailContent emailContent, RegistrationData currentRegData) {
        var currentTimestamp = dateTimeService.getCurrentTimestamp(true);
        if (status.equals(Status.approved)) {
            currentRegData.setApproved(true);
            currentRegData.setApprovalTimeStamp(currentTimestamp);
            emailContent.setMessage("Учетная запись одобрена");
        } else if (status.equals(Status.rejected)) {
            currentRegData.setApprovalTimeStamp(currentTimestamp);
            emailContent.setMessage("Учетная запись не одобрена");
        }
        registrationDataRepository.save(currentRegData);
    }
}
