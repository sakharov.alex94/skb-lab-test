package com.example.test.services.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public abstract class TaskQueue<T extends Task> {

    private static final Logger logger = LoggerFactory.getLogger(TaskQueue.class);

    private final BlockingQueue<T> queue = new LinkedBlockingQueue<T>();
    private final TaskExecutor<T> executor;

    public TaskQueue(TaskExecutor<T> executor) {
        this.executor = executor;
    }

    public void add(T task) {
        queue.add(task);
    }

    //обработка задач из очереди
    public void run() {
        var threadExecutor = Executors.newSingleThreadExecutor();
        threadExecutor.submit(() -> {
            try {
                while (true) {
                    var task = queue.take();
                    executor.execute(task);
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex.getCause());
            }
        });
    }
}
