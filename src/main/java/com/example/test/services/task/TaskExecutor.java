package com.example.test.services.task;

public interface TaskExecutor<T> {
    void execute(T task);
}
