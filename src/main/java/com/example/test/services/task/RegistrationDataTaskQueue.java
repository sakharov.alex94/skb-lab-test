package com.example.test.services.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RegistrationDataTaskQueue extends TaskQueue<RegistrationDataTask> {

    @Autowired
    public RegistrationDataTaskQueue(TaskExecutor<RegistrationDataTask> executor) {
        super(executor);
    }
}
