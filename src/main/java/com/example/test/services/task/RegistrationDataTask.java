package com.example.test.services.task;

import com.example.test.model.MessageId;
import com.example.test.model.RegistrationData;

import java.util.UUID;

public class RegistrationDataTask extends Task {

    private MessageId messageId;
    private RegistrationData registrationData;

    public RegistrationDataTask(UUID id, MessageId messageId, RegistrationData registrationData) {
        super(id);
        this.messageId = messageId;
        this.registrationData = registrationData;
    }

    public MessageId getMessageId() {
        return messageId;
    }

    public void setMessageId(MessageId messageId) {
        this.messageId = messageId;
    }

    public RegistrationData getRegistrationData() {
        return registrationData;
    }

    public void setRegistrationData(RegistrationData registrationData) {
        this.registrationData = registrationData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        RegistrationDataTask that = (RegistrationDataTask) o;

        if (getMessageId() != null ? !getMessageId().equals(that.getMessageId()) : that.getMessageId() != null)
            return false;
        return getRegistrationData() != null ? getRegistrationData().equals(that.getRegistrationData()) : that.getRegistrationData() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getMessageId() != null ? getMessageId().hashCode() : 0);
        result = 31 * result + (getRegistrationData() != null ? getRegistrationData().hashCode() : 0);
        return result;
    }
}
