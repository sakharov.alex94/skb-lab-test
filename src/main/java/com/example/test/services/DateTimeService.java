package com.example.test.services;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public interface DateTimeService {
    LocalDateTime getCurrentDateTime(boolean isUtc);

    Timestamp getCurrentTimestamp(boolean isUtc);
}
