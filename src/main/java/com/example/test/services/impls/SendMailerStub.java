package com.example.test.services.impls;

import com.example.test.model.EmailAddress;
import com.example.test.model.EmailContent;
import com.example.test.services.SendMailer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
public class SendMailerStub implements SendMailer {

    private static final Logger log = LoggerFactory.getLogger(SendMailerStub.class);

    @Override
    public void sendMail(EmailAddress toAddress, EmailContent messageBody) throws TimeoutException {

        if (shouldThrowTimeout()) {
            sleep();

            throw new TimeoutException("Timeout!");
        }

        if (shouldSleep()) {
            sleep();
        }

        // ok.
        log.info("Message sent to {}, body {}.", toAddress, messageBody);
    }

    private static void sleep() {
        try {
            Thread.sleep(TimeUnit.MINUTES.toMillis(1));
        } catch (InterruptedException e) {
            throw new RuntimeException("Sending interrupted!", e);
        }
    }

    private static boolean shouldSleep() {
        return new Random().nextInt(10) == 1;
    }

    private static boolean shouldThrowTimeout() {
        return new Random().nextInt(10) == 1;
    }
}
