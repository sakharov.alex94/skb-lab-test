package com.example.test.services.impls;

import com.example.test.model.Message;
import com.example.test.model.RegistrationData;
import com.example.test.repository.RegistrationDataRepository;
import com.example.test.services.MessagingService;
import com.example.test.services.RegistrationDataService;
import com.example.test.services.task.RegistrationDataTask;
import com.example.test.services.task.RegistrationDataTaskQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Service
public class RegistrationDataServiceImpl implements RegistrationDataService {

    private final MessagingService messagingService;
    private final RegistrationDataRepository registrationDataRepository;
    private final RegistrationDataTaskQueue taskQueue;

    @Autowired
    public RegistrationDataServiceImpl(MessagingService messagingService, RegistrationDataRepository registrationDataRepository, RegistrationDataTaskQueue taskQueue) {
        this.messagingService = messagingService;
        this.registrationDataRepository = registrationDataRepository;
        this.taskQueue = taskQueue;
    }

    @Override
    @Transactional
    public CompletionStage<Void> register(RegistrationData registrationData) {
        var dataByLoginFuture = registrationDataRepository.findByLogin(registrationData.getLogin());
        return registrationDataRepository.findByEmail(registrationData.getEmail())
                .thenAcceptBoth(dataByLoginFuture, (byMail, byLogin) -> {
                    if (byLogin != null) {
                        throw new ResponseStatusException(HttpStatus.CONFLICT, "Пользователь с таким логином уже существует");
                    }
                    if (byMail != null) {
                        throw new ResponseStatusException(HttpStatus.CONFLICT, "Данный адрес электронной почты уже используется");
                    }
                })
                .thenCompose(aVoid -> CompletableFuture.supplyAsync(() -> registrationDataRepository.save(registrationData)))
                .thenAccept(savedRegData -> {
                    //ставим в очередь (TaskQueue) задачу на подтверждение учетных данных
                    var messageId = messagingService.send(new Message<>());
                    var task = new RegistrationDataTask(messageId.getId(), messageId, savedRegData);
                    taskQueue.add(task);
                });
    }
}
