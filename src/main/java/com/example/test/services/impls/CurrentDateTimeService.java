package com.example.test.services.impls;

import com.example.test.services.DateTimeService;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class CurrentDateTimeService implements DateTimeService {

    @Override
    public LocalDateTime getCurrentDateTime(boolean isUtc) {
        return isUtc ? LocalDateTime.now(ZoneOffset.UTC) : LocalDateTime.now();
    }

    @Override
    public Timestamp getCurrentTimestamp(boolean isUtc) {
        return isUtc ? Timestamp.valueOf(LocalDateTime.now(ZoneOffset.UTC)):
                new Timestamp(System.currentTimeMillis());
    }
}
