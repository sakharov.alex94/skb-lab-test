package com.example.test.services;

import com.example.test.model.RegistrationData;

import java.util.concurrent.CompletionStage;

public interface RegistrationDataService {
    CompletionStage<Void> register(RegistrationData registrationData);
}
