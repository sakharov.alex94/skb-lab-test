package com.example.test.services.converters;

import com.example.test.model.RegistrationData;
import com.example.test.model.RegistrationDataDto;

public interface RegistrationDataConverter {
    RegistrationData convert (RegistrationDataDto registrationDataDto);
}
