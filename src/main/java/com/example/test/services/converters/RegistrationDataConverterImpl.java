package com.example.test.services.converters;


import com.example.test.model.RegistrationData;
import com.example.test.model.RegistrationDataDto;
import com.example.test.services.DateTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationDataConverterImpl implements RegistrationDataConverter {

    private final DateTimeService dateTimeService;

    @Autowired
    public RegistrationDataConverterImpl(DateTimeService dateTimeService) {
        this.dateTimeService = dateTimeService;
    }

    @Override
    public RegistrationData convert(RegistrationDataDto registrationDataDto) {
        return new RegistrationData()
                .login(registrationDataDto.getLogin())
                .password(registrationDataDto.getPassword())
                .email(registrationDataDto.getEmail())
                .fullName(registrationDataDto.getFullName())
                .creationTimeStamp(dateTimeService.getCurrentTimestamp(true))
                .approvalTimeStamp(null)
                .isApproved(false);
    }
}
