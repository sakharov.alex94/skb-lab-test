package com.example.test.services;

import com.example.test.model.EmailAddress;
import com.example.test.model.EmailContent;

import java.util.concurrent.TimeoutException;

public interface SendMailer {
    void sendMail(EmailAddress toAddress, EmailContent messageBody) throws TimeoutException;
}
