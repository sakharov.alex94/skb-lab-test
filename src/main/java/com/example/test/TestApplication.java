package com.example.test;

import com.example.test.services.task.RegistrationDataTaskQueue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) {
        var context = SpringApplication.run(TestApplication.class, args);
        context.getBean(RegistrationDataTaskQueue.class).run();
    }
}
