package com.example.test.repository;

import com.example.test.model.RegistrationData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Repository
public interface RegistrationDataRepository extends CrudRepository<RegistrationData, UUID> {

    @Async
    CompletableFuture<RegistrationData> findByLogin(String login);

    @Async
    CompletableFuture<RegistrationData> findByEmail(String email);
}
