package com.example.test.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
@EnableRetry
public class RetryConfiguration {

    @Value("${retry.message-service.maxAttempts}")
    Integer messageServiceMaxAttempts;

    @Value("${retry.message-service.maxDelay}")
    Long messageServiceMaxDelay;

    @Value("${retry.mail-service.maxAttempts}")
    Integer mailServiceMaxAttempts;

    @Value("${retry.mail-service.maxDelay}")
    Long mailServiceMaxDelay;

    @Bean(name = "message-service")
    public RetryTemplate getMessageServiceRetryTemplate () {
        var retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(messageServiceMaxAttempts);

        var backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(messageServiceMaxDelay);

        RetryTemplate template = new RetryTemplate();
        template.setRetryPolicy(retryPolicy);
        template.setBackOffPolicy(backOffPolicy);
        return template;
    }

    @Bean(name = "mail-service")
    public RetryTemplate getMailServiceRetryTemplate () {
        var retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(mailServiceMaxAttempts);

        var backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(mailServiceMaxDelay);

        RetryTemplate template = new RetryTemplate();
        template.setRetryPolicy(retryPolicy);
        template.setBackOffPolicy(backOffPolicy);
        return template;
    }
}
