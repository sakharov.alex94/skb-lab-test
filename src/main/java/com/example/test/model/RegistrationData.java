package com.example.test.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(indexes = {@Index(name = "idx_login", columnList = "login", unique = true),
        @Index(name = "idx_email", columnList = "email", unique = true)})
public class RegistrationData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Column(nullable = false, unique = true)
    private String login;
    private String password;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false)
    private String fullName;
    private Timestamp creationTimeStamp;
    private Timestamp approvalTimeStamp;
    private Boolean isApproved;

    public RegistrationData() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Timestamp getCreationTimeStamp() {
        return creationTimeStamp;
    }

    public void setCreationTimeStamp(Timestamp creationTimeStamp) {
        this.creationTimeStamp = creationTimeStamp;
    }

    public Timestamp getApprovalTimeStamp() {
        return approvalTimeStamp;
    }

    public void setApprovalTimeStamp(Timestamp approvalTimeStamp) {
        this.approvalTimeStamp = approvalTimeStamp;
    }

    public Boolean getApproved() {
        return isApproved;
    }

    public void setApproved(Boolean approved) {
        isApproved = approved;
    }

    public RegistrationData login(String login) {
        setLogin(login);
        return this;
    }

    public RegistrationData email(String email) {
        setEmail(email);
        return this;
    }

    public RegistrationData password(String password) {
        setPassword(password);
        return this;
    }

    public RegistrationData fullName (String fullName) {
        setFullName(fullName);
        return this;
    }

    public RegistrationData creationTimeStamp (Timestamp creationTimeStamp) {
        setCreationTimeStamp(creationTimeStamp);
        return this;
    }

    public RegistrationData approvalTimeStamp (Timestamp approvalTimeStamp) {
        setApprovalTimeStamp(approvalTimeStamp);
        return this;
    }

    public RegistrationData isApproved(Boolean isApproved) {
        setApproved(isApproved);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegistrationData that = (RegistrationData) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getLogin() != null ? !getLogin().equals(that.getLogin()) : that.getLogin() != null) return false;
        if (getPassword() != null ? !getPassword().equals(that.getPassword()) : that.getPassword() != null)
            return false;
        if (getEmail() != null ? !getEmail().equals(that.getEmail()) : that.getEmail() != null) return false;
        if (getFullName() != null ? !getFullName().equals(that.getFullName()) : that.getFullName() != null)
            return false;
        return isApproved != null ? isApproved.equals(that.isApproved) : that.isApproved == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getLogin() != null ? getLogin().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + (getFullName() != null ? getFullName().hashCode() : 0);
        result = 31 * result + (isApproved != null ? isApproved.hashCode() : 0);
        return result;
    }
}
