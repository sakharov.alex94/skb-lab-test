package com.example.test.model;

public class Message<T> {

    private MessageId messageId;
    private Status status;
    private String message;

    public Message() {
    }

    public Message(MessageId messageId, Status status, String message) {
        this.messageId = messageId;
        this.status = status;
        this.message = message;
    }

    public MessageId getMessageId() {
        return messageId;
    }

    public void setMessageId(MessageId messageId) {
        this.messageId = messageId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
