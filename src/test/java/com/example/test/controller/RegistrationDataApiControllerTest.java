package com.example.test.controller;

import com.example.test.model.RegistrationData;
import com.example.test.model.RegistrationDataDto;
import com.example.test.services.RegistrationDataService;
import com.example.test.services.converters.RegistrationDataConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flextrade.jfixture.JFixture;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.concurrent.CompletableFuture;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class RegistrationDataApiControllerTest {

    private final JFixture fixture = new JFixture();
    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private RegistrationDataConverter registrationDataConverter;
    @MockBean
    private RegistrationDataService registrationDataService;

    @Test
    void register_success() throws Exception {
        //Arrange
        var regDataDto = fixture.create(RegistrationDataDto.class);
        var regData = fixture.create(RegistrationData.class);
        var jsonContent = mapper.writeValueAsString(regDataDto);

        when(registrationDataConverter.convert(eq(regDataDto)))
                .thenReturn(regData);

        when(registrationDataService.register(eq(regData)))
                .thenReturn(CompletableFuture.completedStage(null));

        //Act
        var request = mockMvc.perform(post("/registrationData")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonContent))
                .andExpect(request().asyncStarted())
                .andReturn();

        var result = mockMvc.perform(asyncDispatch(request));

        //Assert
        verify(registrationDataService).register(eq(regData));
        result.andExpect(status().isCreated());
    }

    @Test
    void register_badRequest() throws Exception {
        //Arrange
        var regDataDto = fixture.create(RegistrationDataDto.class);
        regDataDto.setLogin("");
        var jsonContent = mapper.writeValueAsString(regDataDto);

        //Act
        var request = mockMvc.perform(post("/registrationData")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(jsonContent))
                .andExpect(request().asyncStarted())
                .andReturn();

        var result = mockMvc.perform(asyncDispatch(request));

        //Assert
        result.andExpect(status().isBadRequest());
    }
}