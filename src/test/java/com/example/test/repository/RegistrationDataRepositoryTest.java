package com.example.test.repository;

import com.example.test.model.RegistrationData;
import com.flextrade.jfixture.JFixture;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class RegistrationDataRepositoryTest {

    private final RegistrationDataRepository repository;
    private final JFixture fixture = new JFixture();

    @Autowired
    RegistrationDataRepositoryTest(RegistrationDataRepository repository) {
        this.repository = repository;
    }

    @BeforeEach
    void clearDataBaseBefore() {
        repository.deleteAll();
    }

    @AfterEach
    void clearDataBaseAfter () {
        repository.deleteAll();
    }

    @Test
    void findByLogin_success() {

        //Arrange
        var regData = fixture.create(RegistrationData.class);
        regData.setLogin("testLogin");
        var expected = repository.save(regData);

        //Act
        var actual = repository.findByLogin("testLogin").join();

        //Assert
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void findByLogin_ActualIsNull() {

        //Arrange
        var regData = fixture.create(RegistrationData.class);
        regData.setLogin("testLogin");
        repository.save(regData);
        //Act
        var actual = repository.findByLogin("testLogin1").join();

        //Assert
        assertThat(actual).isNull();
    }

    @Test
    void findByEmail_success() {

        //Arrange
        var regData = fixture.create(RegistrationData.class);
        regData.setLogin("testEmail");
        var expected = repository.save(regData);

        //Act
        var actual = repository.findByLogin("testEmail").join();

        //Assert
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void findByEmail_ActualIsNull() {

        //Arrange
        var regData = fixture.create(RegistrationData.class);
        regData.setEmail("testEmail");
        repository.save(regData);

        //Act
        var actual = repository.findByLogin("testEmail1").join();

        //Assert
        assertThat(actual).isNull();
    }
}