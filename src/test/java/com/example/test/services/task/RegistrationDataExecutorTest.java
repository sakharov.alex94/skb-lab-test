package com.example.test.services.task;

import com.example.test.model.*;
import com.example.test.repository.RegistrationDataRepository;
import com.example.test.services.MessagingService;
import com.example.test.services.SendMailer;
import com.flextrade.jfixture.JFixture;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
class RegistrationDataExecutorTest {

    private final JFixture fixture = new JFixture();
    private final UUID uuid = UUID.randomUUID();

    @MockBean
    private MessagingService messagingService;
    @MockBean
    private SendMailer sendMailer;
    @MockBean
    private RegistrationDataRepository registrationDataRepository;
    @Autowired
    private RegistrationDataExecutor executor;


    private Function<RegistrationData, Boolean> matchRegistrationDataByIsApprovedTrue = x -> x.getApproved().equals(true) && x.getApprovalTimeStamp() != null;
    private Function<RegistrationData, Boolean> matchRegistrationDataByIsApprovedFalse = x -> x.getApproved().equals(false) && x.getApprovalTimeStamp() != null;

    @Test
    void execute_approved_success() throws TimeoutException {
        //Arrange
        var regData = fixture.create(RegistrationData.class).isApproved(false).approvalTimeStamp(null);
        var messageId = new MessageId(uuid);
        var task = new RegistrationDataTask(uuid, messageId, regData);
        var successfulMessage = new Message<>(messageId, Status.approved, "");
        var emailAddress = new EmailAddress(regData.getEmail());
        var emailContent = new EmailContent("Учетная запись одобрена");

        when(messagingService.receive(eq(messageId)))
                .thenReturn(successfulMessage);

        when(registrationDataRepository.save(argThat(x -> matchRegistrationDataByIsApprovedTrue.apply(x))))
                .thenReturn(null);

        doNothing().when(sendMailer).sendMail(eq(emailAddress), eq(emailContent));

        //Act
        executor.execute(task);

        //Assert
        verify(messagingService).receive(eq(messageId));
        verify(registrationDataRepository).save(argThat(x -> matchRegistrationDataByIsApprovedTrue.apply(x)));
        verify(sendMailer).sendMail(eq(emailAddress), eq(emailContent));
    }

    @Test
    void execute_rejected_success() throws TimeoutException {
        //Arrange
        var regData = fixture.create(RegistrationData.class).isApproved(false).approvalTimeStamp(null);
        var messageId = new MessageId(uuid);
        var task = new RegistrationDataTask(uuid, messageId, regData);
        var notSuccessfulMessage = new Message<>(messageId, Status.rejected, "");
        var emailAddress = new EmailAddress(regData.getEmail());
        var emailContent = new EmailContent("Учетная запись не одобрена");

        when(messagingService.receive(eq(messageId)))
                .thenReturn(notSuccessfulMessage);

        when(registrationDataRepository.save(argThat(x -> matchRegistrationDataByIsApprovedFalse.apply(x))))
                .thenReturn(null);

        doNothing().when(sendMailer).sendMail(eq(emailAddress), eq(emailContent));

        //Act
        executor.execute(task);

        //Assert
        verify(messagingService).receive(eq(messageId));
        verify(registrationDataRepository).save(argThat(x -> matchRegistrationDataByIsApprovedFalse.apply(x)));
        verify(sendMailer).sendMail(eq(emailAddress), eq(emailContent));
    }

    @Test
    void execute_messageServiceRetry() throws TimeoutException {
        //Arrange
        var regData = fixture.create(RegistrationData.class).isApproved(false).approvalTimeStamp(null);
        var messageId = new MessageId(uuid);
        var task = new RegistrationDataTask(uuid, messageId, regData);
        var successMessage = new Message<>(messageId, Status.approved, "");
        var emailAddress = new EmailAddress(regData.getEmail());
        var emailContent = new EmailContent("Учетная запись одобрена");

        when(messagingService.receive(eq(messageId)))
                .thenThrow(TimeoutException.class)
                .thenThrow(TimeoutException.class)
                .thenReturn(successMessage);

        when(registrationDataRepository.save(argThat(x -> matchRegistrationDataByIsApprovedTrue.apply(x))))
                .thenReturn(null);

        doNothing().when(sendMailer).sendMail(eq(emailAddress), eq(emailContent));

        //Act
        executor.execute(task);

        //Assert
        verify(messagingService, times(3)).receive(eq(messageId));
        verify(registrationDataRepository).save(argThat(x -> matchRegistrationDataByIsApprovedTrue.apply(x)));
        verify(sendMailer).sendMail(eq(emailAddress), eq(emailContent));
    }

    @Test
    void execute_mailServiceRetry() throws TimeoutException {

        //Arrange
        var regData = fixture.create(RegistrationData.class).isApproved(false).approvalTimeStamp(null);
        var messageId = new MessageId(uuid);
        var task = new RegistrationDataTask(uuid, messageId, regData);
        var successfulMessage = new Message<>(messageId, Status.approved, "");
        var emailAddress = new EmailAddress(regData.getEmail());
        var emailContent = new EmailContent("Учетная запись одобрена");

        when(messagingService.receive(eq(messageId)))
                .thenReturn(successfulMessage);

        when(registrationDataRepository.save(argThat(x -> matchRegistrationDataByIsApprovedTrue.apply(x))))
                .thenReturn(null);

        doThrow(TimeoutException.class).when(sendMailer).sendMail(eq(emailAddress), eq(emailContent));

        //Act
        executor.execute(task);

        //Assert
        verify(messagingService).receive(eq(messageId));
        verify(registrationDataRepository).save(argThat(x -> matchRegistrationDataByIsApprovedTrue.apply(x)));
        verify(sendMailer, times(3)).sendMail(eq(emailAddress), eq(emailContent));
    }
}