package com.example.test.services.impls;

import com.example.test.model.MessageId;
import com.example.test.model.RegistrationData;
import com.example.test.repository.RegistrationDataRepository;
import com.example.test.services.MessagingService;
import com.example.test.services.RegistrationDataService;
import com.example.test.services.task.RegistrationDataTask;
import com.example.test.services.task.RegistrationDataTaskQueue;
import com.flextrade.jfixture.JFixture;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
class RegistrationDataServiceImplTest {

    private final JFixture fixture = new JFixture();
    private final UUID uuid = UUID.randomUUID();

    @MockBean
    private MessagingService messagingService;
    @MockBean
    private RegistrationDataRepository registrationDataRepository;
    @MockBean
    private RegistrationDataTaskQueue taskQueue;
    @Autowired
    private RegistrationDataService service;

    @Test
    void register_success() {
        //Arrange
        var regData = fixture.create(RegistrationData.class);
        var messageId = new MessageId(uuid);
        var task = new RegistrationDataTask(uuid, messageId, regData);

        when(registrationDataRepository.findByEmail(eq(regData.getEmail())))
                .thenReturn(CompletableFuture.completedFuture(null));

        when(registrationDataRepository.findByLogin(eq(regData.getLogin())))
                .thenReturn(CompletableFuture.completedFuture(null));

        when(registrationDataRepository.save(eq(regData)))
                .thenReturn(regData);

        when(messagingService.send(any()))
                .thenReturn(messageId);

        doNothing().when(taskQueue).add(eq(task));

        //Act
        service.register(regData).toCompletableFuture().join();

        //Assert
        verify(registrationDataRepository).findByEmail(eq(regData.getEmail()));
        verify(registrationDataRepository).findByLogin(eq(regData.getLogin()));
        verify(registrationDataRepository).save(eq(regData));
        verify(messagingService).send(any());
        verify(taskQueue).add(eq(task));
    }

    @Test
    void register_loginAlreadyExist() {
        //Arrange
        var regData = fixture.create(RegistrationData.class);

        when(registrationDataRepository.findByLogin(eq(regData.getLogin())))
                .thenReturn(CompletableFuture.completedFuture(regData));

        when(registrationDataRepository.findByEmail(eq(regData.getEmail())))
                .thenReturn(CompletableFuture.completedFuture(null));

        //Act
        assertThatThrownBy(() -> service.register(regData).toCompletableFuture().join())
                .isExactlyInstanceOf(CompletionException.class)
                .getCause().isExactlyInstanceOf(ResponseStatusException.class)
                .hasMessage("409 CONFLICT \"Пользователь с таким логином уже существует\"");
    }

    @Test
    void register_emailAlreadyExist() {
        //Arrange
        var regData = fixture.create(RegistrationData.class);

        when(registrationDataRepository.findByEmail(eq(regData.getEmail())))
                .thenReturn(CompletableFuture.completedFuture(regData));

        when(registrationDataRepository.findByLogin(eq(regData.getLogin())))
                .thenReturn(CompletableFuture.completedFuture(null));


        //Act
        assertThatThrownBy(() -> service.register(regData).toCompletableFuture().join())
                .isExactlyInstanceOf(CompletionException.class)
                .getCause().isExactlyInstanceOf(ResponseStatusException.class)
                .hasMessage("409 CONFLICT \"Данный адрес электронной почты уже используется\"");
    }
}